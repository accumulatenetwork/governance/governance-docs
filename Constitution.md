# Accumulate Constitution

## Introduction

Accumulate builds upon nearly 7 years of knowledge gained from operating the Factom Protocol and supporting a diverse set of applications for both public and private institutions. Accumulate executes on the original vision of the Factom Protocol to be an enterprise-grade blockchain capable of managing data, identity, and security with the cost, throughput, and flexibility demanded by the traditional business world. Accumulate relies upon the participation of a passionate and engaged community to guide the development of the protocol and advocate for its broad utility across markets.

Accumulate must create a space where anyone with a stake in the protocol can participate in its validation and anyone who wants to build with the protocol has the resources they need to do so. The Accumulate Constitution is the starting point to allow large groups of decentralized participants to coordinate and interface with a world of many decentralized and centralized entities. The constitution is purposefully kept lightweight, as it is intended to serve as a guiding document. The governance approach is outlined [here](Governance.md).

## The ACME Token

ACME is the native token of Accumulate.

- ACME has a hard limit of 500 million tokens.
- Roughly 200 million ACME will exist at the activation of Accumulate’s Mainnet in Q3 2022.
- The unissued token pool will hold roughly 300 million ACME at activation.
- The protocol follows a Burn-and-Mint Equilibrium (BME) model, where any ACME that is burned to perform work on Accumulate are returned to the unissued token pool.
- A token budget is computed roughly once a month using an annual minting rate of 16% of the tokens in the unissued pool.
- The inflation rate halves roughly every 4 years (greater utility as measured by the burn rate of ACME stretches out the halving rate).
- Newly minted tokens from the unissued pool are primarily distributed to protocol validators and stakers.

## Committees and Domains

To avoid voter fatigue of stakeholders in the protocol, the main duty of governance by stakers and validators is to select committees to manage domains.

The following domains are recognized by the protocol:

- **Protocol Oversight and Operations** -- Management of the protocol, operation of the protocol, facilitation of efforts between domains, and vetting of stakeholders.
- **Staking** -- Manage operations of the staking service.
- **Core and Core Service Development** -- Code development for the protocol core, code implementing protocol services, and management of Accumulate Improvement Proposals (AIPs). This includes all layer 1 development in the protocol, the Accumulate Improvement Proposals (AIP), as well as any SDKs/Clients exposing direct functionality of the Accumulate Protocol. Main stakeholders: Core Developers, AIP Authors.
- **Ecosystem Development:** This includes any software solution or open-source product not directly or minimally impacting the core layer of the protocol. Main stakeholders: Integrators, Users.
- **Outreach, Community and Marketing** -- , etc. Main stakeholders: Token holders, Node operators, Core Developers.- Ecosystem Development -- Grants, Community building, outreach, coordination of integrations with devices, companies, protocols, other communities, etc. Main stakeholders: Token holders, Node operators, Core Developers.
- **Exchanges and Liquidity** -- Negotiate with exchanges, provide liquidity in decentralized markets. Main stakeholders: Token holders.

For more information on committees and domains, please see the [Committees](./Committees.md) document.

## Stakeholders

Note: The information below is intended to come into effect after the initial transitional period (see [here](./ProofOfStakeTransition.md))

Stakeholders are responsible for the governance and decisions made in the protocol. Because of the 24/7 nature of managing and running a protocol, most of the daily work is managed by the committees. The committees may recruit and support particular stakeholders for some roles.

The following validator groups are currently recognized as part of the Accumulate protocol. All participants must stake ACME to participate. An ADI and a _Minimum Stake_ of ACME are required to participate in protocol, community, or core services role:

- Node Validators for the Directory Validator Network, and the Block Validator Networks (protocol).
- Data Server Validators (core service).
- Staking Validators (core service).
- NFT Validators (core service).
- ACME stakers (community).

All staking earns tokens. Users who also run active validator nodes will earn a validator bonus on top of staking rewards. Earnings are proportional to the tokens staked. ADIs get one vote regardless of tokens staked.

A Stakeholder is an ADI, and must represent a party distinct from other parties in the network. Mining pools or staking services that provide individual stakeholders the ability to cast their vote in the protocol can manage multiple stakeholders.

In the future, stakers will be able to cast votes on a regular schedule (e.g. once per quarter for the committees, and once per year for larger changes to the protocol). This schedule will be determined by the Governance Committee for Phase II. Very rarely, a special election may be held for some time-critical and protocol-wide issue. In this case, the Governance committee with the help of the foundation would handle the vote.

Voting will be coordinated on the Accumulate network to maintain an immutable public record of proposals and their evaluation by the community.


## Principles

As part of this constitution we have created a set of principles that will guide the development of Accumulate as a network. Whenever decisions are made these principles are expected to be taken into account.

1) Transition is necessary and constant
2) Effective decentralization takes time
3) Each decision should meet criteria of transparency, responsibility and accountability
4) Stakeholders are the ultimate decision makers

### 1. Transition is necessary and constant
Transition is needed to build the foundations to ultimately achieve the Accumulate values.
"Rome was not built in a day." is very much applicable to how a blockchain network is built and operated. We acknowledge that we will need to adapt over time to the needs of the network and it's stakeholders. We cannot foresee everything, nor do we want to. There is a big reasons why for instance in Software Engineering changes these days are made in small increments using Agile techniques.

### 2. Effective decentralization takes time
We have had ample experience with Factom, where in hindsight we made the switch to a fully decentralized model too soon, resulting in all kinds of problems, which mainly was apparent in the lack of effective decision-making and improper handling of grants. We acknowledge that the network starts out decentralized, but it takes time to get every aspect fully decentralized. We do not see that as problematic. The level of decentralization needs to be appropriate for the level of development of the protocol as a whole, we do not strive to decentralize everything as quickly at all costs. This of course highly correlates with the next 2 points.

### 3. Transparency, responsibility and accountability
Since Accumulate will have the concepts of committees responsible for certain domains, it means smaller groups of people are involved in day to day decision-making in these respective domains. This has the benefit of more effective decision-making, but only if that happens in a transparent and accountable manner.  This means that committees and their members will need to report to the wider community, and that the community will need to be able to see the decisions made by the committees. It also means that members of committees and the committees as a whole need to be accountable for their decisions. Stakeholders should be able to remove members of committees if something happens which is not according to the principles and/or mandate of the respective committee or wider ecosystem.

### 4. Stakeholders are the ultimate decision makers
Everyone who has value at stake in the network is a stakeholder. During the transition period stakeholders will not be able to vote on chain yet. After that voting will be possible for certain aspects. Although there are committees that are responsible for certain day-to-day aspects of the protocol, these committees are serving the protocol and its ultimate decision makers: all Stakeholders

