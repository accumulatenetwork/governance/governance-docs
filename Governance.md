# Accumulate Protocol Governance

## Contents

[Governance is decentralized](#governance-is-decentralized)

[Stakeholders](#stakeholders)

[Incentives](#incentives)

[Mechanisms for Coordination](#mechanisms-for-coordination)

[More decentralization over time](#more-decentralization-over-time)

[Governance is split into multiple domains](#governance-is-split-into-multiple-domains)

[Domain allocations](#domain-allocations)

[Domain budget changes](#domain-budget-changes)

[Operators](#operators)

[Operator minimum stake amount.](#operator-minimum-stake-amount)

[Operator set entrance](#operator-set-entrance)

[Operators Stake Short term](#operator-stake---short-term)

[Operator Stake Longer term (18 months)](#operator-stake---longer-term-18-months)

[Operator commission](#operator-commission)

[Staking Pool Commission Short Term](#staking-pool-commission---short-term)

[Staking Pool Commission Medium term](#staking-pool-commission---medium-term)

[Validator states](#validator-states)

[Slashing](#slashing)

[Downtime slashing and pausing](#downtime-slashing-and-pausing)

[No update and pausing](#no-update-and-pausing)

[Double signing](#double-signing)

[Delegators and staking](#delegators-and-staking)

[Delegated and undelegated/pure stake](#delegated-and-undelegatedpure-stake)

[Delegation bonding Warmup and Cooldown periods](#delegation-bonding-warmup-and-cooldown-periods)

[Medium-Long term (12-18 months)](#medium-long-term-12-18-months)

[Lockup period](#lockup-period)

[Short term lockup implementation](#short-term-lockup-implementation)

[Long term lockup implementation](#long-term-lockup-implementation)

[Delegator slashing](#delegator-slashing)

[Saturation](#saturation)

[Staking rewards](#staking-rewards)

[Frequently Asked Questions](./FAQ.md)

## Governance is decentralized

No one person owns or controls Accumulate. Decisions are needed to implement changes that best ensure the longevity and prosperity of the network. A lack of central ownership makes traditional organizational governance not a viable solution. Ideally, all decision-making happens in a fully decentralized manner. Before Accumulate came to be, the Factom protocol was not POS, but Proof of Authority (POA). Lessons have been learned from the complex paper-based governance at the time. People interested in some of these examples can look at Factom's [governance documents](https://forum.factomprotocol.org/).

As seen in other (D)POS systems, Accumulate's governance is controlled by validator nodes in the operator set as well as by any participant that wants to stake (lock) their tokens. The longer one stakes their tokens, the higher the yield (return). Any party that wants to stake their tokens chooses a period of time over which they agree to lock their tokens. See the chapters about [staking](./ProofOfStakeTransition.md) for more information.

## Stakeholders

There are different stakeholders in the Accumulate community, each playing a different role in the governance process

- **Token/ACME holders:** these people hold an arbitrary amount of ACMEs or custom tokens created by an ADI Token Issuer.
- **Users:** these people and parties interact with applications on Accumulate.
- **Integrators:** these people and parties write applications that run on (e.g. DeFi, Identity, anchoring) or create tooling that interacts (wallets, SDKs, applications) with Accumulate.
- **Node Operators:** Also called 'operators', these people run nodes that propagate and validate blocks and transactions.
- **AIP Authors:** these people propose changes to the Accumulate protocol, in the form of Accumulate Improvement Proposals (AIPs).
- **Core Developers:** these people and parties maintain the core network, node software as well as various language implementations.
- **Accumulate Foundation:** The Accumulate Foundation is established in Gibraltar for the purpose of providing a legal entity by which the Accumulate community can interact with the world when required.  For example the Foundation can serve as a legal entity when the community needs to enter into real world contracts. The Accumulate Foundation has little decision making power of its own but rather serves to carry out the decisions of the Accumulate Committees.  The only descretion given to the Foundation is it can refused to carry out committee instructions that would violate applicable laws. 
- **Accumulate Committees:** these committees are responsible for the day to day decision making required to keep the protocol running.  All spending must originate from Accumulate Committees. 

Please note that these roles are not necessarily exclusive. An Integrator can also be and in a lot of cases is an ACME holder. AIP Authors are typically very technical and could be Core Developers for instance.

## Incentives

Each committee in Accumulate has its own incentives, and these incentives are not always 100% aligned. Committees will propose changes over time that are expected to be biased towards their own survival. This manifests itself typically in changes to the reward structures, monetary policy, or balances of power.

### Mechanisms for Coordination

Since it's unlikely to have 100% incentive alignment all the time, the ability for each group to coordinate around their common incentives is critical to affect change. If one group can coordinate better than another, it creates power imbalances in their favor. In practice, a major factor is how much coordination can be done on-chain vs. off-chain, where on-chain coordination makes coordinating easier. In some other new blockchains (such as Tezos or Polkadot), on-chain coordination allows the rules or even the ledger history itself to be changed.

### More decentralization over time

In order to protect the network security as well as resource allocation, Accumulate becomes more decentralized over time. In 2-5 years time, the protocol will move to a 100% on-chain governance model where all decisions are being made by all staking parties. We are doing this over time, because most examples of DPOS networks still have quite a high degree of centralization because of tokens being unevenly split within the network, even for projects that started out completely 'decentralized'.

This translates to core development starting out the most centralized, where core developers and Accumulate Improvement Proposal (AIP) authors are being handled by a core committee making decisions about the actual implementation. Delegators and Node Operators will decide whether or not to update the software. Therefore, the core developers are not solely in control. Over time, the node operators will be approving the AIPs, before implementation starts, meaning an automatic majority support. Because of the highly technical nature as well as the amount of changes needed in the beginning, we start out with a committee of individuals that have deep technical understanding of the protocol and a positive track record.

The core committee consists of 5 members: 2 members from the Core developers team/parties, and one member each from the AIP Author, Node Operator and Integrator roles.

Over time, the protocol will trend towards greater decentralization and more changes will be voted upon by the operators (who run validator nodes) and delegators, eventually involving the whole community. For example, the community may vote on Accumulate Improvement Proposals (AIP) or Grants for applications/solution development. The move from core development towards full decentralization is expected to be slower than the implementation of layer 2 grant proposal voting.

# Governance is split into multiple domains

The Accumulate governance is split into multiple domains. This is done to ensure each domain is allotted adequate resources and does not need to compete with other domains. It will also allow stakeholders to focus on the domains which they will be most impacted by and have the most expertise in. However it is important to note that stakeholders are incentivized to make decisions that benefit the protocol as a whole and all stakeholders. You can compare it to more traditional organizations where there is a budget for Marketing, a budget for Sales etc. More sales can happen because of more marketing, which results in more development. The domains and stakeholders should reinforce each other.

The domains have separate budgets and are managed by committees. Every domain is handled by one committee, but one committee can handle multiple domains. The domains are defined in the [Committees document](./Committees.md#committee-domains).

Each committee defines its own scope, responsibilities and maintains its own charter, ultimately under the scrutiny of the Accumulate community. Each domain will have a separate budget allocated through the community and managed by each committee.

## Committees

We propose the establishment of four (grant) committees before launch:

- Governance
- Core Development
- Ecosystem
- Business

We'll launch each committee with a minimum of 3 members. But as should be expected in a decentralized community, these committees will be autonomous: they will themselves decide how to best run their committee and how to best use their allocated budget.

Most important is that they must be transparent in their decisions, because they exist by grace of the community and ultimately the community (the Accumulate stakeholders - the token-holders) will vote on their performance, (proposed) members and even their ultimate existence.

Every committee must have a chairman. The committee can use internal mechanisms to determine who the chair should be. This is also left up to the autonomy of the committee.

Please see [the Committees section](./Committees.md) for more information.

## Domain allocations

We believe it makes sense to have a system of communicating vessels, where the APY changes over time and excess tokens not spent from a domain can be donated to the staking domain. Whenever a lot of development, outreach and exchange work is happening it should result in price increases, which means a decision for a lower APY could be desirable.

The domain allocations after the [transition period](./ProofOfStakeTransition.md) of 6 months is projected to be up to 20% of the [unissued ACME pool](./ProofOfStakeTransition.md#foundation). As the inflation is a variable, these percentages may change. The actual allocations will be set by the Governance committee.

| Domain | Allocation | Committee |
| ------ | ------ | ------ |
| Protocol Oversight and Operations | 5% | Governance committee |
| Exchanges and Liquidity | 20% | Governance committee |
| Core Development | 30% | Core Development committee |
| Application / Solution Development | 20% | Ecosystem committee |
| Business | 15% | Business committee |
| Marketing | 10% | Business committee |

Each committee independently decides how to best use their allocated budget and the responsibilities that comes with that, such as transparency through reporting and accountability to the Accumulate community.

That also means that committees **can** decide to spend their funds on other things outside the scope of their domain, for example:

- to donate funds to other committees who need a larger budget
- to the staking domain, to increase APY for stakers
- to engage and contract domain experts for advice or other task
- and even to other projects outside of the Accumulate Network

To minimize the chances of cannibalization or short term directional changes, the budgets for the different domains can be changed every 6 months.

# Committee/Foundation Relationship

Decision making power, for the Accumulate protocol, is in the hands of the Accumulate Committees who are charged with working in the best interests of the Accumulate community.  The committees are in charge of day to day decision making for the protocol.  The committees are incentivized to keep the larger communitty's wishes in mind as the community can vote a committee out if the communitte becomes unhappy with the decisions made by a committee.

The Accumulate Foundation has no decision making authority beyond refusing to carry out instructions that violate an applicable law or are deemed unethical. It is up to the foundation if and when they want to bring up these issues with stakeholders. In all other cases the Foundation is to carry out the decisions of the community or the committees.

The Accumulate Foundation will provide the Accumulate Committees with a legal entity which can contract with the real world where required. The Foundation may enter into legal agreements, in the name of the Community, with the authorization of an Accumulate Committee. 

The Foundation is responsible for determining the budget necessary for the Foundation's operations, which is submitted to the governance committee for approval. The governance committee is responsible for approving the Foundation's budget. The Foundation is responsible for submitting a quarterly budget report to the governance committee.

# Operators

Operators are individuals or organizations that run validator nodes. Operators are initially vetted by the core committee. An exception will be made for Factom ANOs, which automatically can become an Operator if the minimum stake value is achieved. To that end, ANOs will receive staking tokens from the Factom grantpool ([see here](./Grants.md#factom-grant-pool)). 

## Operator set entrance

An entity is defined as an individual or organization that is listed as a shareholder. If an entity is a significant shareholder (owning more than 30% of shares) of more than one node operator, only one of those node operators will have signing authority. This is done to prevent a single entity from gaining outsize influence on the network. 

For example, if Investor X has significant ownership of Operator A and Operator B, only one of those operators can have their key included in the operator Key Book.

### Operator Stake - Short Term

Operators are expected to stake at least **50,000 ACME** per validator node in [phase 1](./ProofOfStakeTransition.md#phase-i-delegated-gated-proof-of-stake-gpos). The maximum staking amount is equal to 25% of the current number of validator nodes multiplied by the minimum staking amount, which ensures that one party does not have too much influence.

### Operator Stake - Longer term (18 months)

The protocol allows anyone to become an operator by creating a special transaction to signal the intent from an ADI. The ADI and/or transaction should disclose information about the party. Information about a party can be used by others to determine whether or not they want to delegate their stake to this operator. The voting power is the total stake (one's own stake and delegated stake) of the operator. 

## Operator commission

Operators earn rewards proportionally based on the amount staked. This is inclusive of both the operator's own stake as well as the combined stake of all of the delegators that are contributing to that particular operator's staking pool. Operators will ultimately be able to set a commission rate on their respective staking pool. However, to ensure that competition between operator staking pools does not lead to an immediate "race to the bottom", there will be a phased approach to the rollout of commissions: short term, medium term, and long term.

### Staking Pool Commission - Short term

A fixed 10% commission rate will be enforced for all operators running validator nodes, Follower Nodes or Staking Nodes, meaning any users who delegate their stake to an operator will receive 90% of their proportion rewarded (APY).

### Staking Pool Commission - Medium term

Operators will be allowed to set their own commission rate. This will allow operators to be more competitive when they require a higher stake from stakers, or if they would like to lower the amount staked in their pool due to the diminishing returns of higher stake amounts.

There will be restrictions in place to prevent operators from increasing their commission rate too much at one time - i.e. a max increment per time period.

## Validator states

- **Active:** The validator is part of the validator set and is currently validating transactions and generating yield for itself and its bonded stakers. It is able to vote as part of the consensus protocol.
- **Eligible:** The validator is part of the validator set, but not actively validating or generating yield. It is able to vote on proposals and grants, but is not able to vote as part of consensus.
- **Paused:** The validator is not part of the validator set and is not generating yield. The validator was removed from the validator set by operator choice or because of an external event, regardless of whether it has met the minimum stake requirement. External events that can lead to pausing include but are not limited to long downtimes, failure to update the software, failure to vote. A paused validator needs to send a `resume` transaction to return to the validator set.
- **Evicted:** The validator is permanently removed from the validator set by the network. Reasons for this include but are not limited to malevolently behavior. A larger percentage of the stake of the operator and delegators are slashed. An evicted validator cannot rejoin the validator set. Stakers bonded with an evicted validator are free to bond with new validators without a cool-down period.
- **Terminated:** The validator removes itself from participation in the network, and is not part of the validator set. This happens for instance when the operator decides to cease operations. Once terminated the validator cannot rejoin the validator set for 30 days.

## Slashing

Slashing is the process of losing a percentage of stake. This applies to both operators as well as delegators. This is done to bind them together in a common goal of being honest, providing stability and security to the network. Slashing not only has economic consequences but also governance consequences as the total stake of the operator (including all of its delegators) is reduced as a result of the slashing event. When slashing occurs, the tokens are sent to a dedicated address of the Governance Committee. The committee then either uses that address for [slash reversals](#slash-reversals), in case the slashing was deemed erroneous by the committee, or it will be added to a domain of their choice, meaning not necessarily a domain controlled by the Governance Committee.

### Slash reversals
If an operator believes that their stake has been slashed erroneously, they can submit a request for review to the Governance committee. Causes for undeserved slashing could for instance include network-wide bugs, global network/ISP outages or other events outside the control of the operator.

If the Governance Committee judges that the stake has been slashed due to a network bug or some other mistake, it will put up the consideration to the staking parties and redeem the slashed stake(s).

### Downtime slashing and pausing

If an operator is not participating in the network for some period of time, that operator is liable to a slashing penalty. Operators are expected to have an uptime of 98% per month, which translates to a maximum downtime of 14.5 hours. If a validator node is down for more than this percentage the node will be Paused (see operator states) for an increasing amount of time, meaning it will not participate in earning rewards for itself and its delegators anymore. The initial minimal Pause is 10 minutes, and doubles every time. The node operator will need to send a `resume` transaction to become part of the Candidate/Validating set again. These measures ensure the Node operators stay on top of their game with regards to technical monitoring and responsiveness. It also means that if they do not take action immediately they are not generating rewards for a longer time. The stake being slashed is 0,01% per occurrence. That includes the stakes of delegators, so it is in the best interest of delegators to choose reliable and technical operators, which in turn implies that operators that are more public about their team/capabilities probably have an edge over anonymous teams. The minimum Pause of 10 minutes doubles every time it has been slashed for this reason. It halves again every 3 months if it did not occur, to ensure an operator does not get penalized for eternity for events that happened in the past. Whenever an operator is paused, delegators are allowed to re-assign their remaining stake to other operator(s). Re-assigning does not change the slashing of the delegator however.

### No update and pausing

This happens if 51% of the operator set has voted in favor of an update to the latest version and a particular operator hasn't voted in favor (this includes non-voting). Operators ultimately determine whether or not they agree to implement changes by updating their nodes. Any nodes not taking part in the vote, or voting against the update will be _paused_ in every case where the vote has reached a majority of 51% when the voting period ends. The operators will be paused until the moment that all operators who voted in favor have updated their nodes or the activation + update-period has passed, whichever comes first. An operator can only get back to the active set as soon as its version is in line with the voted upon update.

If an operator does vote in favor but hasn't updated their validator node(s) in 7 days after the activation, they will get paused for 1 day. In order to get back to the active set, the update has to take place and the operator needs to create an unpause transaction to participate again.

If an operator does not agree with the update, but 51% of operators have voted in favor, the operator can decide to leave the operator set and send a termination transaction. This means it will not be generating rewards anymore. Both the operators and the delegators can decide to re-assign their stake, but the 7 day timeout is still applicable to protect against bad actors. Parties can also decide to reclaim their stake and claim rewards. Normal slashing rules apply in this case. If an operator does not seem to respond to updates, delegators can choose to re-assign their stake to another operator. An operator that has been paused needs to send a `resume` transaction to participate again. If the version of the software has not been updated the operator would immediately be paused again.

Note: This only works if there is one implementation of the node software, or if we have a feature set activation at the protocol level.

### Double signing

A wrong setup of the validator node(s) or loss of keys (theft), can lead to double-signing. This is a serious security offense for the network and the penalty for it is severe as well. Double signing leads to 25% slashing of the stake, including the loss of all staking rewards. Next to that the operator is evicted, and thus cannot rejoin the operator set anymore. The slashing is also applicable to delegators. Delegators can only reassign or re-claim their stake after 7 days, at which time normal staking timeouts apply.

# Delegators and staking

One of the goals of staking is to lower the velocity of tokens that are circulating. This helps with more predictability. The goal is to reach 60%-80% of circulating supply being staked. Higher numbers might impact liquidity in more negative ways, resulting in more price volatility and/or people not even wanting to transact tokens.

## Delegated and undelegated/pure stake

There are 2 staking methods possible in Accumulate:

- The first staking method is undelegated staking, sometimes also referred to as pure stake. This type of staking can be initiated and ended at any moment in time. There is no concept of penalties, or having to bond/delegate your stake. There are no warm-up or cool-down periods. You can participate in voting. It is the simplest way to earn some rewards (yield). The yield percentage is the lowest however, but it gives you maximal flexibility. The initial APR is estimated to be 4%.

- The second method is delegated staking, meaning that you bond as a delegator with an operator. The rewards (yield) is higher and it allows you to vote. Typically, the operator votes on your behalf, but in the future you will also be able to vote yourself. Delegated staking has warm-up/cool-down periods and slashing, which are explained elsewhere. The yields are higher the longer you decide to bond your stake with an operator, but there is also the possibility of a penalty in case you choose to bond with an operator who does not perform as required. The projected yield is 16%.

### Transition period

The rollout of Accumulate will take place in 2 phases, an initial Gated Proof of Stake phase, followed by a Proof of Stake phase with automated slashing and other features. The first phase is estimated to last around 6 months, dependent on the complexities of implementing the required features. In order to reward early adopters, phase 1 will include staking features to provide yield to token holders. See [Staking Rewards](./ProofOfStakeTransition.md#rewards) for more information.

## Delegation bonding Warmup and Cooldown periods

When bonded delegation is activated, reassigned or deactivated, it does not take effect immediately. A bonding delegation activation or deactivation takes 7 days to complete. This is to protect against potential attacks.

### Medium-Long term (12-18 months)

Fractional (de)activation aka (de)vesting schedules for delegations; meaning having a certain percentage activated every X blocks linearly. The benefit is that stakers start staking early and that deactivation also results in some recoup of the stake early on.

Overall network limits for total (de)activation result in rebalancing the time it might take, typically in a negative way, to ensure we do not see a snowball effect and allow others to prepare and/or counter in case we hit these limits.

Note: Solana has this as an additional protection against bad actors.

## Lockup period

Delegated stakes are locked, which prevents the tokens from being withdrawn before that particular date has been reached. While locked up, the staking account can still be bonded and re-bonded. Only withdrawal into the wallet address is not allowed. If one chooses to still do that it means 40% of your rewards will be slashed on average. Rewards are only allocated as long as stakes are bonded with an operator.

One can choose to delegate stake between 3 months and 24 months. The longer you stake the higher the rewards percentage will be.

### Short term lockup implementation

Stakes can be locked which prevents the tokens to be withdrawn before a particular date has been reached. While locked up, the stake account can still be bonded or unbonded and rebonded. Only withdrawal into the wallet address is not allowed without a penalty. If one still chooses to withdraw, then 40% of their rewards will be slashed, on average.

### Long term lockup implementation

Operators can decide what the lockup duration will be (this always has to be greater than a global minimum), together with maximum staking periods (and rewards). This means we introduce more competition. For this to work, we need an open admission of operators first.

## Delegator slashing

Delegators have to choose the operator(s) they are staking with (bonding) carefully. In the future, operators can set their own rules for lockups as well as commission, impacting the rewards for the party that wants to delegate. Next to that, both a operator and a delegator can get slashed in their stake collectively if the operator is not performing (e.g. downtime) or acting maliciously (e.g. double signing). Therefore, it is vital that a delegator chooses the best operators. If the team and its uptime statistics are public, it can help delegators in making a decision. The amount at stake by the operator is also an indication of the amount of commitment they have in ensuring the network stays stable. For more info see also the slashing chapters for operators.

### Incentives for running multiple validators

To improve the robustness of the network in its early days, an operator will be allowed to operate multiple active core validators, up to a maximum of one active DN validator plus one active BVN validator per BVN. The network will be activated with three BVNs, so at activation the maximum is four active validators per operator. In the future, to incentivize an operator running multiple active validators, each additional active validator an operator runs may increase their overall reward rate.

**This is a temporary measure that will expire after three months.** At that time, the governance committee will decide whether to extend the incentive or to allow it to expire. If enough operators have joined the network such that it would be stable if every operator was running a single active validator, the governance committee may decide to let this measure expire. If they do extend it, it will be extended for another three months, after which they must decide again whether to extend or let it expire.

Additionally, in the future **the governance committee may decide to prohibit an operator from running more than one active core validator.** Once the incentive measure has expired, if the governance committee decides that the network would be better served by each operator running only a single active core validator (increasing distributedness), they may decide to impose a rule to that effect. In that scenario, operators running multiple active core validator nodes will be given one month to decide which validator will remain active. Once a month has elapsed, the govnernace committee will instruct the operators to deactivate the chosen validators. If an operator does not choose, the governance committee arbitrarily choose which validators to shut down.

![](images/APR-100.png)

# Frequently Asked Questions

The FAQ can be found [here](./FAQ.md)
