# Disaster Recovery (proposal)

(Note:- As the State Sync Snapshotting, Data Exporting and Rollback Functionalities are in development, all the commands mentioned in this document are just the placeholders)

## What is Disaster Recovery

Disaster Recovery is a methodology in the Blockchain to reconstruct the Decentralized Byzantine Fault Tolerant network and recover the Distributed Ledger State after events like natural disaster, cyber attacks, or economical attacks. Disaster Recovery is a group of strategies to recover network basing on the technical & economical conditions of the network.

## How does Disaster Recovery work ?

Disaster Recovery depends upon duplication of data and processing power in an node operator not impacted by the disaster. When server breaks down because of Cyber attack, Natural Disaster, Hardware Failure or Application Bug, the Network needed to recover from the last snapshot. Ideally, from that snaphot the complete Network should be recovered and restarted from the last block. There are will be a Dedicated Nodes running for the Disaster Recovery secenarios, where there whole job is to secure the Network state and creating the snapshots in a continuous regular intervals.

---
## Types of Procedures for Disaster Recovery

1. State Sync Snapshotting
2. Rollbacking Network
---
## State Sync Snapshotting

State Sync is a process through which a new node can catchup with the Network by simply fetching all the historcial blocks and start syncing from the last block state. This is a Fastrack process for every new node to reach the latest data byte of the network.

Snapshot Data Storage is the functionality of the Node to export the Data of the Network into a single Database. 

The snapshotting is designed for node operators to run a efficient validator service on Accumulate Network. To make the snapshot as small as possible while still viable as a validator, we use the following setting to save on the disk space. We suggest you make the same adjustment on your node too. Please notice that your node will have very limited funcationality beyond signing blocks with the efficient disk space utilization.

Since we periodically state-sync our snapshot nodes, you might notice that sometimes the size our snapshot is surprisingly small.

app.toml
```
# Prune Type
pruning = "custom"

# Prune Strategy
pruning-keep-recent = "100"
pruning-keep-every = "0"
pruning-interval = "10"
```

config.toml
```
indexer = "null"
```
### Creating a Snaphot

Along with the automatic creation of the snapshots, This is a CLI command to create the snapshots manully.

```
accumulated snaphot export state --height <height> --directory <directory_path>
```
Note:- By default, this cmd will export from the latest height recorded

#### -> <b>Why you should create a snapshot ?</b>

As a validator or follower node operator, to keep your uptime, rewards and backup option. As a discipline, if the validators who keep there Uptime relative to 100%, will be having the great probability of earning the Delegators Trust. As more trust a Validator had, brings more delegations and staking power. As more staking power with Validator, the probablity of mining a block increases. If the Validator mined more blocks than others, it gives more yield. So, We suggest to keep dynamic snapshot creation everytime. This helps to quick sync with the network at any situation.

With dynamic State Sync Snapshot creation process, everything is automated. The Validator or follower needed to just configure the node. It will automatically create a new snapshot for every minor block mined and deletes the old snapshot to minimize the memory size of the Node.

#### -> <b>Who can create a snapshot ?</b>

Any node operator either a Validator or Follower, can create a snapshot. But that node should be having atleast 1 block to create a snapshot.

#### -> <b>How often a snapshot should be created ?</b>

It is completely the node operator choice, We suggest to create the snapshot for every minor block mined.

#### ->  <b> When should a State Sync Snapshot to be created ?</b>

Ideally, State Sync Snapshot should be created for every minor block mined. It's just an optional. But in the case of Disaster Occurancy, the State Sync Snapshot should be created till the [n-1] block. Here, "n" is the latest block height.


### Recovering from a Snapshot

To fast track the process of loading the node to latest network height, the node operator can download the snapshot archives and load the node with this command

```
accumulated snapshot load state --directory <directory_path>
```

#### -> <b>Why is Rollbacking the network important? What problem does it solve?</b>

Rollbacking is one of the fastly executable strategy to recover the node from Cyber Attacks/Network Breakdowns. This solves to quickly remove a certain blocks from the network and start it again.

#### -> <b>When does this procedure take place?</b>

This procedure should only implemented if there is a App Hash Error or Cyber Attack or Network Breakdowns.

#### -> <b>Who is supposed to run this procedure? Can any Node operator run it, or must it be coordinated between all the operators?</b>

Any Node Operator can run this at there own nodes. But, we will be providing a auto triggering mechanism to run this dynamically at every node in coming releases. For now, this is a manual step, the Node Operators should execute this command manually.

#### -> <b>When should a Node Operator run this command ?</b>

These are few list of scenarios, where the node operator can use this command:-
1. If a Operator want to transfer his node from one machine to another.
2. Due to internal/external computation error of Node, if the memory disk of the Node is corrupted and unrecoverable, then Operator can clean the complete disk and use this command to recover his node.
3. A new operator who wants to sync up with the latest network height at the lightening speed, can use this command to load all the Block States.

### Verificiation of the Snapshot

As there can be many spam snapshots the node operators can get through, this is a command to validate and verify the snapshot. 

```
accumulated snapshot verify state --directory <directory_path>
```

#### -> <b>What is a Spam Snapshot ?</b>

Spam Snapshot is a Snapshot which will be an identical twin to the Orginal Snapshot. But, in this there will be few additonal transactions and altered transactions. If the 2/3rd of the network participates tries to use this spam snapshot and start the network. Then, there are high chances of creating a Network Fork. To avoid these attacks, every node operator should verify the snapshot downloaded before connecting with the network.

---
## RollBacking Network

As similar to the Upgrading of the network basing on the developments of the protocol. Rollbacking is process to downgrade the network if the network came across the bad commits.

RollBacking will overwrites the current state of the Network with the most recent previous state (height n-1) or the given height.

#### -> <b>How to Rollback the Network </b>

Command to rollback the Network to any height. Suggested way to stop the complete network operator nodes and commit a specific height to rollback the network

```
accumulated rollback --height <height>
```

#### -> <b> How to Stop the Network </b>

This command can be utilized by any node operator to stop the node at a target height. This helps in the process of upgrading or downgrading the network.

```
accumulated stop --height <height>
```

Note:- Currently, by default the rollbacking will downgrade the network to [n-1] height.

#### -> <b> How to start the Network after a Rollback </b>

Command to start the network

```
accumulated start
```

---
## Genesis Exporting

Exporting genesis is the process of aggregating all the final states of the accounts in the network till the given height.

### Exporting the Genesis File

Command to export the genesis state at a specific height

```
accumulated genesis export state --height <height>
```

#### -> <b>Who is supposed to run this procedure? Can any Node operator run it, or must it be coordinated between all operators? </b>

This process is independent, can be performed at any Node Operator.

#### -> <b>Is the “Rollbacking” step a dependency of this step? I.e. must you first run Rollbacking before you run Genesis Exporting? Can I run Genesis Exporting if another party ran the rollbacking step? </b>

No, Rollbacking is not dependent on this step. This is only execute as a supporting step in State Sync Snapshotting.

#### -> <b>Why is this important? What problem does this solve?</b>

This will be exporting the current network states like Account Details, Account Balances, Transaction Hashes and Deployed Applications Details into a single file, this helps for the node to get all the details in one go . With the Snapshot and the exported genesis file, the node operators can start syncing with the Network from the last block.

### Validation of Genesis File

Command to validating the genesis file before start of the network

```
accumulated genesis validate state --file <genesis_file_path>
```

---
## Final Verification of Recovery

Command to verify all the loaded configurations, data files, key files and genesis files are valid

```
accumulated verify
```
---
## Procedure for Recovery of the Node

Reset your node. WARNING: This will erase your node database. If you are already running validator, be sure you backed up your `priv_validator_key.json` prior to running these commands.

```
accumulated unsafe-reset-all
```

#### -> <b>Why should you implement node resetting? What happens if I don’t reset my node?</b>

Reset the node will complete erase all the details of node. If there is state error or memory disk corruption, then the node operator can perform this step to start again from the first. This can be performed by any Node Operator. 

It is complete depends on the issue at the Node, if there is no Data Corruption, then resetting your node is an optional.


### Complete list of steps to recover your node

```
wget http://accumulate.files.com/snapshots/latest.tz
accumulated snapshot verify state --file <file_path>
accumulated snapshot load state --file <file_path>
wget https://accumulate.files.com/genesis.com
accumulated genesis validate state --file <genesis_file_path>
accumulated start
```

(Note:- The above mentioned process is only for recovering a node with Snapshot, Rollbacking is not required in this process. )

---



