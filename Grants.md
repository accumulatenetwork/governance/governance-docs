# Accumulate Grant System

## Introduction

For Accumulate, we envision a grant system that is:

- **Structured and flexible at the same time**: Providing some form of control to best benefit the Accumulate Network, but open for all parties and all the time.

- **Efficient**: Establishing committees for specific domains with knowledgeable committee members to guide and decide on grant proposals.

- **Transparent**: Providing transparency on the committee member election process, the grant proposal decision process, the grant proposal voting process, and the grant execution and payment process.

### Structured and flexible at the same time

Accumulate is a decentralized network, where there is no central party that is in control. However, we need a system where there is sufficient direction – based on the needs of the Accumulate Network – on what the grantees should work on and deliver. Using Committees for the grant work based on the actual needs of the Network is an efficient way to use the grant pool.

On the other hand, external parties will likely come up with some great ideas that we will not have thought about. These will also bring value to Accumulate.

From experience we've seen that there are often small grant requests for specific purposes that are time-critical, e.g., speaking opportunities, hackathons, subscriptions, etc., We need to be flexible enough to accommodate these as well.

## Three types of grant programs

There are three types of grant programs:

1. **Accumulate Open Calls**
   - For Core Development

     - These grants are aimed at extending the core functionalities of the network, typically based on [Accumulate Improvement Proposals (AIPs)](https://gitlab.com/accumulatenetwork/aip)
     - The Core Development committee can publish a Request for Proposal (RFP) for certain core functionalities that they deem as required.
     - Any party matching the published requirements can submit a proposal.
     - The evaluation, decision, allocation and monitoring are done by the Core Development committee

   - For the Ecosystem (Applications, Tools, Solutions or Products)

     - These grants are aimed at building Applications, Tools, Integrations, Solutions, Products, etc., on top of Accumulate
     - The Ecosystem or Business committee can publish an RFP to develop certain core functionalities they deemed as required. Any party matching the published requirements can submit a proposal.
     - In addition, any party can submit a proposal.
     - The committee will evaluate the proposal and decide if the proposal should be put up for voting by the network stakeholders.


2. **Grant Requests from the community**

   - These grants can be about anything benefiting Accumulate.
   - Any party can submit a proposal.
   - The Ecosystem or Business committee will evaluate a proposal and decide if the proposal should be put up for voting by the network stakeholders.


3. **Fast-track Grant Requests**
   - These types of grant request proposals are typically small and for specific purposes that are time critical, e.g., speaking opportunities, hackathons, subscriptions, etc.
   - The Business committee will evaluate a proposal and decide if the proposal is approved and how it will be monitored.


## Interim grant request application process
The below grant request application process will be used for grant requests pre mainnet launch and end when the [transition period](./ProofOfStakeTransition.md) ends. Then we will move towards a more permanent process.

Firstly, Grant requests can be done at any time and by any person, entity or consortium. It is an open and continuous process.

1. The applicant's contact person will send the grant request to the appropriate committee chairman or a person appointed by the committee.
- In case there is no chairman or contact person, the grant request can be sent to the chairman or contact person of the Governance committee.
- In case a grant request would not qualify for a certain domain/committee, that committee would simply return it to the applicant(s) and ask for clarification or forward it to the proper committee.
- In case there would be a disagreement between committees and/or applicant(s) about which committee must decide on the grant request, the Governance committee will act as a mediator. In cases where committees do not reach a mediated agreement, the Governance committee will provide a binding decision on which committee must process the grant request.
2. The responsible committee will score the grant request and approve or reject the grant request.
- See: [Decision-making](#decision-making)
- If rejected, the responsible committee will provide the applicant(s) with the reasons for rejection.
   - Decisions by the committee can not be appealed, but a grant request can be resubmitted taking into account a 6-month waiting period after the decision.
- If approved, the responsible committee will:
   - inform the applicant of the decision and the procedural steps and conditions for executing the grant, including, but not limited to, milestone reporting, monitoring, auditing, payout, and any other conditions.
   - make the grant request, scoring, decision and conditions public (method tbd)


We therefore establish four grant committees in line with the overall Accumulate governance to manage the Open Calls and streamline the evaluation of the proposals:

_Note. At some point, we might create a larger community voting process for grant requests above a set size or monetary value but, for now and during the [transition period](./ProofOfStakeTransition.md), the committees will handle the approval process of grant requests._


### Grant request budget denomination
The budget in the grant request may be denominated in ACME tokens or in USD. If the grant is denominated in USD, the committee which reviews the grant has the authority to set the ACME/USD conversion using any calculation deemed appropriate by the committee.


### Budgets

The initial Accumulate Grant Pool will be allocated 60 million ACME tokens. Thereafter, the Accumulate Grant Pool will be funded annually by a certain amount of ACME tokens as defined in Governance.

The max supply in Accumulate is 500 million ACME, of which 200 million ACME are circulating after the genesis event. This leaves 300 million unallocated (the unissued pool), of which yearly 16% (48 million ACME in the first year) will be divided between Staking rewards and the Grant Pool after the [transition period](./ProofOfStakeTransition.md). During the [transition period](./ProofOfStakeTransition.md) all inflation (16%) will be allocated to Staking rewards.

 It is estimated that up to 20% of the tokens minted from the unissued pool will be dedicated to the grant pool, with the remaining 80% going to Staking Rewards, upon full implementation of Proof of Stake in Accumulate (see [here](./ProofOfStakeTransition.md)). The Governance committee will propose increases or decreases if there is a need. It tries to keep the allocation of Staking rewards in the range of 80% to 100% of the unissued pool.

The budget distribution will be:

- Governance committee 25%
- Core Development committee 30%
- Ecosystem committee 20%
- Business committee 25%

#### Budget Re-allocation

The budget distribution is intended to remain stable and there is no regularly scheduled review period of the allocations to the different committees. However, if a committee's allocation is at risk of being depleted at a much faster rate than other committees, then that committee can submit a request to the Governance Committee to re-allocate the budget distribution. 

The Governance Committee shall seek the input from all other committees on any changes to the budget - however the Governance Committee holds full authority to enact any changes to the budget. 

Budget re-allocation cannot occur more frequently than once in a six month period.

### Factom Grant Pool

The current Factom Authority Node Operators (ANOs) have over the years all contributed to the Factom Grant Pool.

Since August 2020, the Factom ANOs have agreed not to apply for grants from the Factom Grant Pool, except to fund a few necessary commitments that are needed to keep the Factom Protocol up and running. As a result the Factom Grant Pool is growing. As of October 2022, it holds over 800,000 FCT (or 4,000,000 ACME).

As Factom is transitioned into Accumulate, we want to redistribute all of the deferred FCT pro rata to Factom ANOs who will run validator nodes on Accumulate as node 'operators'. Each ANO that runs at least one validator node on Accumulate will receive a proportional share of the ACME in the Factom grant pool.

The actual amount of ACME will be calculated at the genesis event of the Accumulate Network mainnet as:

- the total amount of FCT in the Factom grant pool at that time,
- multiplied by 5 (the FCT/ACME exchange ratio), and
- divided in equal parts over the number of Factom ANOs that run a Validator node at that time.

The resulting amount of ACME must be used for operator staking with a minimum lock-up period of 6 months.

In order to be eligible for this distribution, **Factom ANOs must be running as an Operator on the Accumulate testnet prior to the mainnet activation date**, currently estimated to be at the end of October 2022.

## Grant governance

Decisions by a small group of people require governance and transparency to provide Trust in the grant process.

### Public stakeholder voting on all grant proposals

In principle, all proposals for grants should be public. We say 'in principle', because the proposal may contain sensitive information that could harm the interest of the applicant if it becomes public too early. This is for the Applicant to decide.
The proposal can be made public at any time by the Applicant, but will be made public at the time of voting by network stakeholders on the proposal.

### Decision-making

Decisions by the committee to put the grant through for voting, or not, will be made by the committee members based on several minimal requirements and the number of points awarded by the committee members, who act as evaluators.

For example:

| Criteria                                         | Evaluation                            |
|--------------------------------------------------|---------------------------------------|
| 1. Fits the pre-requirements<sup>(*)</sup>       | (Yes/No)                              |
| 2. Benefits the ecosystem                        | Score (0.0 - 5.0) - Threshold 3.0     |
| 3. Excellence &amp; Innovation                   | Score (0.0 - 5.0) - Threshold 3.0     |
| 4. Expertise and excellence of the proposed team | Score (0.0 - 5.0) - Threshold 3.0     |
| 5. Project planning and value for money          | Score (0.0 - 5.0) - Threshold 3.0     |
| **TOTAL**                                        | **Score (0.0-20.0) - Threshold 13.5** |

(*) Pre-requirements can be applicable for Accumulate Open Calls. You can see those type of grants like a Requirement For Proposal (RFP) that typically state certain requirements that have to be met by an applicant to qualify for a grant.

Thresholds: a grant request must have a minimum average score of 3.0 on all criteria and a minimum overall average score of 13.5/20.0 (67.5%) in order to qualify for approval. An average score above all the threshold does not mean the grant request is automatically approved: that is a committee decision.

Decisions by the committee can not be appealed, but proposals can be resubmitted as 'Community Grant Applications' after a 6-month waiting period.

### Voting

After an approval of a grant request by a committee, that committee **must** announce this to all other committees through their chairpersons (the Announcement), using the recommended communication channels (Discord).

Any committee has the right to object to this approval. In case a committee wants to object, it **must** do so within 10 working days of the Announcement date. This objection **must** be in writing and **must** contain a motivation of their objection(s). 
Any committee can also state it has no objections at any time within this 10-day period.

In case there are no objections within this 10-day period - or in case all committees state they have no objections - the approval of the grant request automatically stands.

In case there are one or more objections, the original committee **must** call for a meeting between all the committees within 20 working days of the Announcement. In that meeting the objection(s) will be discussed and a vote **must** be taken with the following two options:
1. The grant request approval decision is confirmed.
1. The grant request approval decision is denied.
2. The grant request approval decision is withdrawn for re-evaluation.

Each committee will have one (1) vote, but the original committee will have two (2) votes. A simple majority vote shall decide whether the grant request approval is confirmed or denied. 

In case a grant request approval decision vote is to withdraw, the original committee can re-evaluate the grant request with the grantee - and change the grant request to address the objection(s) - and re-approve the grant request.

In case a grant request approval decision vote is to deny, the grant request is not approved. and returned to the grantee.

Note. This objection/approval mechanism does **not** apply to Fast-track Grant Requests.

### Monitoring of grants

The committee can decide to appoint one or more **Independent Consultant**(s) to supervise an awarded grant. The Consultant(s) will support the grantee during the execution phase of the grant, will report to the committee on the progress and will consult with the committee members on milestones, results, payments, completion, or cancellation of grants.

It is left up to the committees whether Consultant(s) will receive a monthly remuneration in ACME tokens.

## Grant programs summary

1. **Accumulate Open Calls**

   - This program will issue RFPs for proposals for specific Accumulate Improvement Proposals (AIPs), goals/targets, challenges, proposals, tools, or projects.
   - They will be predefined in duration and budget (up to X amount paid).
   - They can be issued continuously, at any time in the year.
   - All proposals will be pre-screened by the committee and must meet all listed RFP prerequisites.
   - A proposal must have measurable milestones. These will be the trigger for payments, if and when they are achieved and signed off by the committee.
   - There will be a structured decision process for each RFP, decided on by the committee.
   - The number of proposals that are awarded can vary per RFP: up to any number of proposals, but also none can be awarded.
   - The committee will decide about the monitoring of the execution when awarding a grant proposal: either by the committee itself or by 1 or 2 qualified independent consultants.

2. **Community Grant Requests**

   - This program will allow continuous proposal submission for grants.
   - Grants can be for any topic, duration, or amount.
   - They will be semi-structured, with some minimal requirements, such as clear, quantified work-packages and (payment) milestones.
   - There will be a structured decision process.
   - The committee will decide about the monitoring of the execution: by the committee itself or by 1 or 2(?) qualified independent consultants.

3. **Fast-track Grant Requests**

   - This program will allow continuous proposal submission for grants.
   - Grants can be for any topic or duration.
   - Up to a maximum amount of $ 5,000 in ACME.
   - Payment can be requested up-front or after completion or a mix.
