# Committees

We need a system that is efficient. A continuous process that does not take too much time and where decisions are made in a fair manner by a rotating group of people that have proven domain knowledge.

Accumulate will start out with 4 committees in line with the overall Accumulate governance.

The committees are:

1. A **Governance committee**, domains:
    1. Protocol Oversight and Operations
    2. Staking
    3. Exchanges and Liquidity

2. A **Core Development committee**, which will manage and decide on RFPs and proposals on:
    1. Core and Core Service Development

3. An **Ecosystem committee**, which will manage and decide on technical oriented grant proposals from anyone about:
    1. Ecosystem Development

4. A **Business committee**, which will manage and decide business- and community-oriented RFPs about:
    1. Outreach, Community and Marketing

## Committee Responsibilities

1. Review grant applications related to the committee's domain
2. Respond to issues and questions arising within their domains of responsibility (as outlined [below](#committees-and-domains))
3. Create [requests for proposals](./Grants.md#three-types-of-grant-programs) to meet needs presented by protocol operations or in response to community feedback
4. Build a framework and process for grant execution tracking.

Each committee will have at least 3 members. As should be expected in a decentralized community, these committees will be autonomous: they will themselves decide how to run their committee. Most important is that they should be transparent in their decisions, but they exist by grace of the community and ultimately the community can vote on their performance and even their existence. Each committee will have a rotating membership of Accumulate stakeholders, i.e. token-holders, who are responsible for voting on grant applications related to the committee's domain.

Every committee needs to have a chairman. The committee can use internal mechanisms to determine who the chair should be. This is left up to the autonomy of the committee.

Each committee will maintain its own charter that documents the administration of the committee, the committee's responsibilities, the definition of the committee's domain, and the grant application review and approval process. The charters can be found [here](https://gitlab.com/accumulatenetwork/governance/grant-committee-charters)

## Committee Domains

To avoid voter fatigue of stakeholders in the protocol, the main duty of governance by stakers and validators is to select committee members to manage domains.

The following domains are recognized by the protocol:

| Domain | Scope | Main stakeholders | Committee |
| ------ | ------ | ------ | ------ |
| **Protocol Oversight and Operations** | Management of the protocol, tokenomics and strategy, facilitation of efforts between domains, and vetting of stakeholders. Also manage operations of the staking service. | Node operators, Token holders, Integrators, Core Developers. | Governance |
| **Exchanges and Liquidity** | Manage (new) exchange listings and provide liquidity in decentralized markets | Token holders | Governance |
| **Core Development** | This includes all layer 1 development in the protocol, the Accumulate Improvement Proposals (AIP), as well as any SDKs/Clients exposing direct functionality of the Accumulate Protocol. | Core Developers, AIP Authors. | Core Development |
| **Application / Solution Development** | This includes any technical proposal, integration, or tool, developed as open-source on top of Accumulate, but not directly or minimally impacting the core layer of the protocol. | Integrators, Users. | Ecosystem |
| **Business** | Promotion and development of end user or partners Solutions or Products on top of Accumulate. | Users, Integrators, Token holders, Node operators. | Business |
| **Marketing** | Community- and marketing outreach, via website, social channels, newsletters, events| Users, Integrators, Token holders, Node operators. | Business |

Domains themselves will be managed by committees, and facilitated by the Governance Committee.

Each domain has its own budget in tokens, and is established annually with a ⅔ majority in favor of change. Committees can give back unused domain tokens, thus reducing inflation.

If required, domains can be added or removed. Added domains could become the responsibility of an existing committee or a new committee can be created for the respective domain. A single committee can be in charge of one or more domains. A domain is always managed by a single committee. The Governance Committee is responsible for adding or removing domains.

### Foundation and committees

A non-profit foundation will handle the grant pool tokens on behalf of the committees. It is important to note that the foundation itself cannot decide how to allocate committee-designated tokens. The committees are in charge of how their respective tokens are handled. They instruct the foundation to execute upon their request.

### Committee members

Each committee must have at least 3 members. As should be expected in a decentralized community, these committees will be autonomous: they will themselves decide how to run their committee. Most important is that they should be transparent in their decisions, as they exist by grace of the community and ultimately the community can vote on their performance and even their existence. There will be a rotating membership with the Accumulate stakeholders, i.e. token-holders, voting on the committee and the members.

Every committee must have a chairman. The committee can use internal mechanisms to determine who the chair should be. This is left up to the autonomy of the committee.

Committee members will be appointed for a 12-month period. The election or re-election event for a new committee member or a new term for a sitting committee member will be published two months before the election date. During a period of up to one month before the election date, any network stakeholder can propose a new candidate for a specific committee. The candidate has to confirm standing for the committee and must provide a candidate introduction document. During the one month before the election, data network stakeholders can vote on the proposed candidates. The candidate with the most votes is elected. In case of a tie in votes received, the sitting committee members will choose the new member with a simple majority vote.

The first appointed members will be appointed for a 6, 12 and 18-month period, so that in the future there always will be a spread of new and experienced members.

### Committee member election

The respective committee itself is responsible for elections of new members. The committee can use internal mechanisms to determine who the chair should be. This is left up to the autonomy of the committee. Any vacant position needs to be posted using the official channels of the protocol (i.e. the [Accumulate Discord](https://discord.gg/bg9yWvArcP) and [Accumulate Twitter](https://twitter.com/accumulatehq)), within eight (8) days of the position becoming vacant. The announcement needs to be made eight (8) days, before the actual election vote is being held. 

Entities interested in joining the committee should vocalize this intent publicly via Discord. During [Phase I](./ProofOfStakeTransition.md) the respective committee will hold a vote (eg. simple majority vote) to determine the new committee member(s). In case of a tie the committee can ask the governance committee for guidance. In case the matter is not resolved, the committee defers to the governance committee for the deciding vote. After the transition period, a stakeholder vote will be held to determine the new committee member(s).

In case the respective committee fails to announce the election or fails to come to consensus in the timeframe mentioned above, the governance committee will step in and facilitate the process for the election of new committee member(s).

### Conflicts of interest

A conflicts of interest is defined as any situation in which a committee member is in a position to potentially exploit their official capacity in some way for their gain or benefit either as private person or as (part of) any entity.

In case there is a proposal that includes an organization or person that has a relationship with a committee member, that committee member will need to make that fact known.

If an organization submits a Grant proposal to one of the committees, and a voting member of that committee is a representative of that organization, then that voting member must recuse themselves from that vote. If, in effect, that committee no longer has a quorum (i.e. a simple majority) to hold a vote, then that committee can elect temporary membership to a member of a different committee in order to complete the vote.

### Conflicts between committees

As domains are handled by a single committee each, there should be no conflicts between committees ideally. In case a conflict does happen between committees, these committees will ask another committee as a mediator to resolve the conflict.

An example would be the allocation of a submitted grant proposal, that might fall into more than one domain. The committees might not necessarily agree on which committee should handle the application.

In case of a conflict, the conflicting committees will ask a 3rd committee to help out to decide which committee should handle the application. In case there is no agreement reached, the governance committee (because of their task of Protocol Oversight and Operations) will issue a binding decision.
