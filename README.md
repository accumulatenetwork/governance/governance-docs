# Accumulate Governance

Welcome to the Governance repository of Accumulate. This is the place where Governance related documents are being maintained. It is also the place for Governance related Frequently asked Questions.

**Note:** These documents are still in draft stage at this point in time.

## Transition period after mainnet launch

Accumulate is the culmination of a lot of work. One of the key features is that Accumulate will be Proof of Stake. In order to get Accumulate out of the door and in the hands of users the decision was made to start the mainnet with a Gated Proof of Stake (GPoS) model, with some aspects of Proof of Stake, such as being able to stake as a token holder, moving into full Proof of Stake with automated slashing within a few months. You can read more about it [here](./ProofOfStakeTransition.md).

## Constitution

The lightweight [constitution](./Constitution.md) that serves as the guiding principle for (future) Accumulate governance, It can be found [here](./Constitution.md).

## Inaugural Governance document

The inaugural Governance document can be found [here](./Governance.md). It explains the set of suggestions for the Accumulate Network including topics such as staking, voting, delegation, validators. The rules laid out in this document are managed by the Governance Committee. This document can be updated at anytime at the Governance Committee's discretion.

## Committees and domains

Committees are responsible for one or more domains. Domains are logically grouped topics which have been identified by the community for which work needs to be done to properly operate and govern the network. More info can be found [here](./Committees.md)

## Suggested Grant System

The suggested [grant system](./Grants.md) can be found [here](./Grants.md). It explains how tokens are reserved and allocated to vital parts of the Accumulate ecosystem, like core development, marketing, exchange listings.

## FAQ

[Frequently Asked Questions](./FAQ.md) can be found [here](./FAQ.md).
