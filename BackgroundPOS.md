# Proof of Stake (Work in Progress)

This document discusses Proof of Stake (PoS) as implemented in Accumulate, and its road map to improving POS over time.

To frame Accumulate's use of PoS, some context of what it does in a Distributed Autonomous Protocol (DAP) can help.  Every DAP requires a set of components:
* A Distributed Ledger: All participants accept the ledger as the state of the protocol
* Databases:  A mechanism that provides access to the historical state of the ledger
* Proofs: Generally cryptography that proves the state of the ledger and elements in the ledger 
* Participants:
  * Developers - maintain code 
  * Validators - run the code to build and distribute the ledger
  * Servers - Distribute and index the ledger
  * Workers - provide interfaces to users, data, resources outside the DAP
  * Managers - protect, maintain, organize, and direct 
  * Communicators - distribute information between validators, users, and the market 
* Code that defines how validators do their job
* An Incentive in the market to drive participation
  * Open Source, where the historical record is code.  The code is the value.
  * Crypto Projects, where the value is defined by a token
    * Proof of Work (PoW)
    * Proof of Stake (PoS)
    * other sorts of proofs 

The topic of DAPs are an alternate means of organizing labor, resources, and the distribution of rewards as an alternate way to organize a defined ecosystem within the global economy.  The topic is very rich and as yet currently unstudied and underappreciated. The topic will not be analyzed in full here.

Accumulate uses gates Proof of Stake where we leverage Accumulate Digital Identities (ADIs) to provide weight in governance to offset the power afforded to large holders in a traditional PoS implementation.

Accumulate also implements staking as an Accumulate service. The advantages include: 
* Ability to update staking rules in the service implementation without impacting core code
* Greater visibility and participation in staking rules
* Ability of other Accumulate services to leverage staking infrastructure 
* Leverages Accumulates Key Book security for services
    * Enhances Accumulate's scalability and performance
* Exercises Accumulate's key book security model for long running services
* Greater review periods for distributions to address attacks and errors
* Simplicity 
* Flexibility in implementing slashing
    * Supports slashing for real world offenses (that cannot be detected in code)
    * Allows for dispute mitigation

Putting all functionality, including staking, into the core code may have some security advantages. The Accumulate designers and developers opted for the scalability, flexibility, and simplicity of the Accumulate service model.