# Governance: Frequently Asked Questions (FAQ)

These are Frequently Asked Questions about Accumulate Governance. If you have questions do not hesitate to reach out.

## Contents

[What is an operator?](#what-is-an-operator)

[What is a delegator?](#what-is-a-delegator)

[What is the minimum amount an operator needs to put at stake?](#what-is-the-minimum-amount-an-operator-needs-to-put-at-stake)

[Do operators stake their personal funds, and if so, is there a limit to how much can be staked?](#do-operators-stake-their-personal-funds-and-if-so-is-there-a-limit-to-how-much-can-be-staked)

[How are operators selected/eligible?](#how-are-operators-selectedeligible)

[What is the APY/APR when staking?](#what-is-the-apyapr-when-staking)

[Can I increase my APR?](#can-i-increase-my-apr)

[A projected 20-25% staking APR is nice, but can I increase that value?](#a-projected-8-9-staking-apr-is-nice-but-can-i-increase-that-value)

[Project X has a way higher APY, are there plans to increase the APY?](#project-x-has-a-way-higher-apy-are-there-plans-to-increase-the-apy)

[How do I receive my stake awards?](#how-do-i-receive-my-stake-awards)

[What is the difference between delegated and undelegated/pure stake?](#what-is-the-difference-between-delegated-and-undelegatedpure-stake)

[What is the inflation percentage?](#what-is-the-inflation-percentage)

[Are you Proof of Stake?](#are-you-proof-of-stake)

## What is an operator?

An operator is a validator node that helps in the consensus and block building in the Accumulate Network. They have to put an amount of ACME at stake in order to become an operator. More info about operators can be found on the [website](https://accumulatenetwork.io/learn/#protocol)

## What is a delegator?

A delegator is a party who places its stake with a (candidate) operator. A delegator's stake is added to the own stake of the operator as well as the stake of other delegators bonded with that operator. Collectively it is called the 'total stake' of the operator. 

## What is the minimum ACME amount an operator needs to put at stake?

At activation and for the duration of [Phase 1](./ProofOfStakeTransition.md#phase-1-proof-of-authority), the minimum stake required by an operator will be **50,000 ACME** for each validator node. Prior to the rollout of [Phase 2](./ProofOfStakeTransition.md#phase-2-proof-of-stake), the [Governance Committee](./Committees.md#committees) will readjust the minimum stake based on the USD price of ACME. This will be done to ensure that the USD-denominated cost for a candidate operator to join the network will reflect the market price of ACME.

## Do operators stake their personal funds, and if so, is there a limit to how much can be staked?

Yes, see the question about the minimum amount an operator needs to put at stake, which is called their 'own stake'.

Besides an operator's 'own stake', there are also the delegators that choose an operator. This is the 'delegated stake'. Combined total own stake and delegated stake is called the 'total stake'. In order to ensure that delegators are incentivized to seek other operators and to protect the network from centralization, there may be an S-curve rule implemented in the future to diminish returns as the total stake increases.

## If I stake my tokens, can I get access to them?

Before May 5, 2023: If you stake your tokens, they will be subject to a mandatory lock-up period for the first 6 months, where you cannot withdraw your tokens.

After May 5, 2023: You are permitted to withdraw staked tokens before the 6 month lock-up period is over, but those tokens may be subject to an early withdrawal penalty. After the mandatory lock-up period, users have the right to add optional locking and mandatory locking to increase their APR.

## How are operators selected/eligible?

During launch, operators are vetted by the core committee, until the number of operators is sufficient for a particular BVN. This is done to ensure that the network sees a proper distribution and has technical parties operating the validator nodes.

If the network and stake are properly distributed we will move to a system where any party that has enough 'total stake' will automatically become an operator from the candidate operator pool. Current projection is that this will take 6-18 months after mainnet launch.

## What is the APY/APR when staking?

The APR is dependent on many factors. The more people stake, the lower the overall APR will become. The target is to have 60%-80% staked within the network. This is a nice balance between having a relatively low velocity and ensuring there is enough liquidity within the network.

In the picture below the above range is depicted by the yellow 125M and green 150M lines. We start at year 1 so to not make extravagant claims about APR in the first few months, as this is obviously higher. APY includes compounding effects of your yields. The Percentage below is the actual yearly APR, and thus doesn't include compounding (APY), which will be higher.

We expect the APR to be around 30% in the first few months, and roughly 25% thereafter, based upon 150 million tokens being staked. These are projections and are dependent on many factors.

Below are several scenario's for the APR. So the Yield represented per year. It shows different lines, which represents the amount of tokens being put up for staking. The more tokens are being staked, the lower the APR will be per staked token. The first few months will start out with 100% of the inflation being allocated to staking, meaning we predict the top picture. After that the Governance committee will decide 

The picture below shows 100% of the inflation being allocated to staking, which will happen the first few months (see [Proof of stake transition](./ProofOfStakeTransition.md)).

<img src="images/APR-100.png" width="500px" alt="100% inflation allocated to staking"/>

The next picture is 80% of the inflation being allocated to staking, which we expect to be the lower bound (see [Proof of stake transition](./ProofOfStakeTransition.md)).

<img src="images/APR-80.png" width="500px" alt="80% inflation allocated to staking"/>

## Can I increase my APR?
Yes. During the transition period all parties staking for at least 6 months, will get their proportional share of the inflation, which is expected to be around 30% initially. If you set up a validator node you will earn a 10% commission on any stake delegated to your node. Both on your own stake, but also on the stake of your delegators.

To increase your returns you can also Pure Stake. Pure staking will allow you to stake your tokens with no risk of slashing, and will also allow you to accept delegated stake. 

## A projected 20-25% staking APR is nice, but can I increase that value?

Yes, there are several ways to increase your APR after the transition period. The longer you lock up your stake the higher your relative rewards ([APR](https://en.wikipedia.org/wiki/Annual_percentage_rate)) will be. The 20-25% projection is the average of all stakers. During the transition period the APR percentage is the same for everyone. With the exception of operators (see previous question)

Next to longer lockups there will also be possibilities to provide liquidity. Liquidity pools, PegNet and other initiatives will allow you to lend ACME tokens to others and get a return on it in the form of transaction rewards. These initiatives will follow after the mainnet launch.

## Project X has a way higher APY, are there plans to increase the APY?

First, we talk about an [APR](https://en.wikipedia.org/wiki/Annual_percentage_rate) value, which depends on factors like network usage, amount of value being staked and staking periods chosen. APR doesn't include compounding effects. A lot of projects talk about the [APY](https://en.wikipedia.org/wiki/Annual_percentage_yield) value, which includes interest compounding on interest. APY is always higher than APR for positive interests/yields.

Furthermore, a bit of a warning: Economics dictate that having extreme yields means that something else has to give. You cannot 'print' money at an unlimited scale, if not properly handled it will be at the expense of the price. As long as a project is still growing, this might not be much of a problem. But at one point it will reach levels at which the high rates will become unsustainable, leading to loss of liquidity and token value as holders exit en masse. The recent collapse of LUNA/UST (i.e. the [LUNA 'Death Spiral'](https://en.wikipedia.org/wiki/Terra_(blockchain)#Collapse)) is a good cautionary tale of what unchecked yield dynamics can lead to.

The Accumulate Governance Committee will consistently monitor network metrics, such as network usage, total amount at stake, number of operator, and network distribution. Percentages can always be adjusted in the future. Additionally, there will be projects within the Accumulate ecosystem that will allow you to increase your yield.

## How do I receive my stake awards?

Rewards are distributed automatically after your chosen lockup period. After the period expires you can create a claim transaction. The staked tokens, including the reward/yield will be transacted to you immediately. If you decide to unstake before the lockup period a cooldown period is taken into account. This is to protect the network against parties that would move stakes around constantly to try to game votes/stakes. Please note that you should create the claim transaction as soon as possible after the lockup period expires, as you will not be receiving any rewards after that period, unless you restake.

## What is the difference between delegated and undelegated/pure stake?

Delegated stake means you will be bonding with an operator. Delegated stake has several pros and cons.

The pros are:

- You get much higher yields, which increase linearly over time
- You are participating in the voting process by following the operator you are bonded with
- In the future you can vote yourself as well
- You are helping to secure the network

The cons are:

- There are lockup periods involved. If you decide to undelegate before the lockup period ends your rewards will be partially slashed
- There are penalties in case the operator you are bonded with misbehaves, meaning you have to pick a competent operator/team

## What is the inflation percentage?

The inflation pays for staking rewards, as well as for the grant pool, from which for instance core development, marketing and exchange listings are being paid by the protocol. The inflation percentage is fixed at 16% of the unallocated tokens. The hardcap of Accumulate is 500 million ACME, with an initial supply of 200 million ACME. Using a fixed percentage of unallocated tokens means that the absolute issuance of tokens for inflation would diminish over time. However, Accumulate has a burn and mint model, meaning usage of the protocol results in ACME being burned. That could result in a circulating supply that stays constant or even decreases over time. So the better the protocol performs in terms of usage, the more resources will be allocated towards staking rewards, development, outreach and marketing. The projection is that initially we will see circulating supply increase, but at one point it will become stable or even decrease.

## Is Accumulate Proof of Stake?

The rollout of Accumulate will take place in 2 phases. The first phase of Accumulate will utilize a Gated PoS model in order to protect/vet the network during the initial rollout of mainnet, as well as to give the developers time to implement all the features needed to enable full Proof of Stake. See [Proof of Stake Transition](./ProofOfStakeTransition.md) for more information.
