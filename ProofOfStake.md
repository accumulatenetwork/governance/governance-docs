# Accumulate Proof of Stake

The implementaiton of the Staking Service in Accumulate
## Types of Staking Accounts and Minimum Staking Amounts
### Full Staking Accounts
Users have a number of options in setting up Staking Accounts:
* Pure Staking Accounts allow users to have full governance rights, but are not obligated to actively participate in the operation of the protocol.
* Protocol Validator Accounts run Accumulate Nodes that validate, order, and execute transactions
* Protocol Follower Accounts run Accumulate Nodes that hold the entire historical data set for the protocol.  At some point in the future, these nodes will become Protocol Data Servers
* Staking Validator Accounts are responsible for the Staking process.
### Delegated Staking Accounts
* Delegated Stakers delegate most of their voting rights to one of the Full Staking Accounts listed above.

### Required Account Minimums:
* **Full Staking Accounts: 50,000 Acme**
* **Delegated Staking Accounts: 1000 Acme**

Once Acme establishes a market price, the minimums will be adjusted to  values specified in USD.  This price will be used to regulate the number of validators in the protocol based on the number required and the demand for participation.

## Staking Account details and options

* Staking is done by ADIs to define the participants to be rewarded tokens
* Gated ADIs are restricted to representing unique entities participating in staking
    * Setting up multiple gated ADIs (running Full Staking accounts) by a single entity is subject to slashing
    * Delegated staking is not gated.  An entity can run as many delegated accounts as they wish.

* To be a Validator or Server in Accumulate, an entity must put up a bond.  
    * This bond is a Full Staking account and defines the entity's role in the protocol.
    * Bonded accounts add a Role weight to the staked tokens used in the calculation of rewards
        - ```Role Weight = 30%```
    * A validator can be slashed for protocol failures or performance failures
    * A server can be slashed for performance failures
* Pure Staking accounts are not bonded
    * No risk of slashing
    * Provides equal standing with Validators and Servers in governance of the protocol
    * Because no participation is required, the Role weight for Pure Staking accounts carry no added Role Weight:
        - ```Role Weight = 0%```
* Rewards are distributed to a user specified rewards account
    * Rewards paid to a non-staking account are immediately available to the holder without penalty, regardless of any locking options that might be specified by the Staking Account
    * Rewards paid to a staking account are treated as a deposit to the staking account. 
    * Any token account on any ADI can be specified as the Reward Account for a Staking Account
* Staking Accounts can specify Locking of the tokens, as well as a Hard Locking of tokens.
    * All Locking options apply to tokens as they are deposited
    * When Locking options expire on a deposit, the tokens are paid out to the reward address. 
        * If the reward address is the same staking account, the tokens are not moved, but the current locking options are reapplied
        * No interruption of rewards occurs
    * Locking creates incentives to hold tokens for longer time periods
    * Locking is specified in quarters (13 week periods)
    * Lock weight 
        - ```Lock Weight = number of quarters locked * 1%```
    * Maximum locking period is 3 years, or 12 quarters
    * The Maximum added weight would be 12% for 12 quarters
    * Locking weight can be changed by the account holder
        * The current locking weight applies to deposits
        * Different deposits can have different locking weights within the same staking account
        * Changing the current Locking Weight does not change the Locking Weight applied to past deposits
    * Early access to tokens can be allowed with a penalty
        * This is not true if Hard Locking is specified on the account
        * Tokens are always withdrawn first in first out
            * Hard locked tokens are skipped in this calculation
        * Penalty of early withdraw is 40% of returns to be paid by tokens
        * Penalties are paid by additional tokens removed from the account
        * Withdraws are not allowed to exceed the tokens in the account
    * Hard Locking can be selected for a greater weight
        * Hard Locking prevents any early access to tokens
        * Hard Lock Weight
            - ```Hard Lock Weight = number of quarters locked * .5%```
        * Hard Locks apply to deposits
        * Hard Locking applies to the locking period in place. If no locking period is specified on an account, no hard locking can be applied
        * The Maximum added weight would be 6% for locking periods of 12 quarters
        * Hard Locks can be removed from an account or added
        * Hard Locks apply to deposits made while hard locks are active
        * Changes to Hard Locks do not change the locks that were applied to deposits in the past
* Restricted Locking Period 
    * October 31, 2022 - May 5th 2023
    * Any additional Locking period specified during the Restricted Locking Period extends the Locking and Hard Locking of tokens in the account.  
        * Example:  Adding Locking of 2 quarters during the Restricted Locking Period means all tokens in the Staking Account are Hard Locked for 1 year.
    * All tokens deposited during the Restricted Locking Period are
        * Locked and Hard Locked for 26 weeks from the first full pay period after the deposit
        * Tokens that are Locked and Hard Locked past the Restricted Locking Period are weighted according to Locking and Hard Locking status for 2 quarters.
    * Early withdraws are prohibited
    * Tokens staked in the first 6 months get a Hard Lock until May 5th 2023
    * Creates market security during the activation of the Accumulate update
    * Tokens deposited in the first 6 months are Locked for 6 months
        * After May 5th 2023, early withdraws are enabled
    * Locking and Hard Locking can be added to accounts after May 5th 2023
* Delegation allows anonymous staking 
    * Delegated staking does not require a gated ADI
    * Delegated staking delegates to another full staking account
    * Most decision making is delegated to the full staking account
    * Delegate accounts get the same Role Weight as the full staking account
    * A delegate account chooses its own Locking Weight and Hard Lock weight
    * Delegate accounts share any risks of slashing defined by the full staking account's role
    * 10% of Delegated returns are paid to the full staking account
    * The delegate can refuse to lend standing in governance to the full
        * Refusing the standing does not give the delegate voting rights
    * The full staking account for a Delegated staking account can be changed at any time
        * Changing the full staking account requires the new staking account's approval
            * While waiting on the new staking account's approval
                * Role for the delegate continues to be dictated by the current full staking account
                * Governance continues to be delegated to the current full staking account
        * Changing the full staking account updates the Role Weight for the delegate account
        * Voting rights of the delegate afforded to the current full staking account are ended on the next pay day
        * Voting rights to the new full staking account are added on the next pay day
* Deposits
    * Changes to Accounts made in this payment period become the pending changes for the next payment period
    * Pending changes to Accounts made prior to the current payment period are applied
* Rewards to the staking accounts
    * Pay period is defined as Friday UTC through Thursday UTC
    * Pay day is the day rewards are distributed to Stakers
        * Pay day occurs on Friday (the first day of the next pay period)
        * Rewards are distributed on tokens that have been in the staking account for a full pay period
        * Withdraws are processed after one full pay period
    * The first pay period after activation starts 25 November 2022
    * The first pay period after activation is 2 December 2022

## Possible Future Staking options

* Bonding 
    * Role weight may be further adjusted for different roles to balance participation
        * costs of different roles may justify different returns
    * Bonding weight may be bounded for some adjustments
        * returns to cover costs may not justify applying returns to total staking amounts
* Mining
    * Allows distribution of the historical record
    * Ensures completeness of the historical record
    * Provides permissionless participation in the protocol
    * CPU mining uses 90% less power than traditional PoW
* Aged Representation
    * Staking accounts can be aged by date of deposit 
    * Older tokens can carry greater representation than younger tokens
    * Discourages wealth attacks
        * Deposits can be noted when made, investigated
        * Detected wealth attacks can be responded to before damage is done

## Reward calculations

As seen by the description above, certain options provide different returns on the same tokens staked.  The staking service uses the following process:

1. On First of Month
    1. A weekly budget is calculated (If on Pay Day, this is done first)
        1. The total supply of issued tokens is calculated 
        1. The unissued tokens is calculated: 
            - ```Unissued = 500,000,000 - Total Supply ```
        1. Yearly Budget:
            - ```Yearly Budget = Unissued * 16%```
        1. Weekly Budget:
            - ```Weekly Budget = Yearly Budget / 365.25 * 7```
2. On Pay Day
    1. Apply any Account updates to each account
        1. Changes to Account roles
        2. Changes to Account Locking
        3. Changes to Account Hard Locking
        4. Changes to Delegate's full staking account
    1. For each staking account
        1. Balance
            -  ```Balance = prior Balance + Deposits made in the Pay Period```
        2. Weighted Balance
            - ```Weighted Balance = Balance * (1 + Role Weight + Lock Weight + Hard Lock Weight)```
        3. Delegate Contribution (what a delegate provides the full staking account)
            * Calculated only on Delegate Accounts
            - ```Delegate Contribution = Balance * (1+Role Weight)*10%```
        3. Delegate Weighted Balance
            * Calculated only on full staking Accounts
            - ```Delegate Weighted Balance = Sum the Delegate Contribution of all delegates to this account```
    3. Compute the Total Weighted Balances
        - ```Total Weighted Balances = Sum the Weighted Balance of all accounts```
    4. Compute the Weighted Reward (the reward paid to each weighted token)
        - ```Weighted Return = Weekly Budget / Total Weighted Balances```
    5. Compute the Account Reward for each account
        1. Full staking accounts
            - ```Account Reward = (Weighted Balance + Delegate Weighted Balance)*Weighted Return```
        2. Delegate accounts
            - ```Account Reward = (Weighted Balance - Delegate Contribution)*Weighted Return```


In order to allow for a timely launch of the Accumulate Mainnet, we will manually process staking with a public spreadsheet. Some simplifications may apply.

## Deployment

The Accumulate Staking Service will be launched in two phases.

- Phase I: Restricted Proof of Stake
- Phase II: Unrestricted Proof of Stake

Phase I will place locking and hard locks on all deposits until after the first 6 months of Accumulate activation.  After May 5, 2023 tokens can be withdrawn with the early withdraw penalty.  During Phase I the Staking Service App will be deployed in stages to automate and adjust the Staking process.

Locking and Hard Locking can be set on staking accounts, and Locking Weights and Hard Locking Weights can be added, and update returns.

## Initial Staking Process

1. Entities create an ADI with a token account
2. Entities submit the ADI along to the governance committee on staking
    * Select restricted locking period
        * 2 quarter minimum
            * Lock Weight and Hard Lock Weight is ignored for 2 quarters in calculating         
        * Additional Locking 
            - Starts after the 2 quarter minimum
            - 12 quarter (3 year) maximum additional locking
            - Hard Locking
            - Lock Weight and Hard Lock Weight is included in weight calculations
    * rewards 
        * After the restricted locking period, locking is treated like a longer locking period with a hard lock.
    * For Full Staking accounts
        * Includes information to demonstrate independence of other staking ADIs
        * Delegate Acceptance Policy
            * Will accept without vetting
            * Approval required (must provide an email)
            * Refuses all 
    * For delegating accounts
        * Indicates a Full Staking account
            * May defer to the Accumulate Foundation
            * Confers governance authority (yes/no)
    * Optional Source of funds
        * Emails of one or more entities that will fund the account (Funding entities may require Additional Locking)
3. Governance Committee on Staking adds a record to acc://stacking.acme/approved account
    * ADI
    * Additional Locking
    * Delegate Acceptance Policy (n/a for Delegate Accounts)
    * Full Staking Account (n/a for Full Staking Accounts)
    * Governance conferred [n/a] (n/a for Full Staking Accounts)
    
